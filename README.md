# Nobel Prize Prediction

Yating Wu, Makie Maekawa, Sonia Sun, Thibault de Courson

## Motivation

Due to the most recent Nobel Prize results, there are two interesting facts that stand out for us.
First, the Nobel prize winner in the economic area got fired last year in a Chinese university since he failed the standard exam.
Although it is possible that this is a rumor, it is not the first time that a Nobel prize winner failed a standard exam.
In 2021, Tomas Lindahl, who was awarded the 2015 Nobel Prize in Chemistry, admitted that he failed his chemistry class in high school.
Hence, if the score or standard test score is not a good indicator for predicting Nobel prize winner, what will it be?

Second, after Japan announced their plan for winning 30 Nobel Prizes over 50 years in 2000, there is a significant increase already.
In the past 20 years, Japan already had 20 Nobel Prize winners, so our group is also wondering what did Japan do to earn this result.

## Data Cleaning

As we did not find any dataset corresponding to our analysis, we have to build one ourselves using various sources.
The data cleaning tool, thus, a big part of our project.

For the dependent variable, we used the list of the Nobel Prize laureates offered by the website opendatasoft.com(1).
It contains 975 Nobel Prize laureates for the 5 categories of the prize: Physics, Chemistry, Medicine, Peace and Literature.
These laureates are spread over the 118 years the Nobel Prize has been delivered between 1901 and 2021 (the Nobel Prize was interrupted during the Second World War).

![](img/stem_laureates_per_country.jpg)

For our analysis we decided to focus on the STEM Nobel Prizes (the Chemistry, Physics and Medicine categories) as we believe they are influenced by factors more similar than the other categories.
As we only have country data after 1959 we also removed the Nobel Prizes won the previous years.
The filtered dataset contained 419 laureates, with an average of 2.3 laureates per prize.

To define the country to which each laureate, we used the column "Organization country", as we believe the country he worked in is more deserving of the prize than the country he emigrated from (more details in the section Nobel Prize attribution to countries of the appendix).
We obtained a list of nineteen countries having won a Nobel Prize.

For our features, we used the WorldBank DataBank(2), which offers eighty five databases containing thousands of indicators providing information on most of the countries of the world.
However a major downfall of this abundance of features is that they contain a lot of missing data.
In order to easily find the relevant indicators, we created a program generating a pre-selection (more details in the section Selection of relevant indicators in the appendix), eliminating the indicators having more than 30% of missing data.
We obtained a list of 533 indicators that we used to choose 20 indicators to use.

We imported the chosen indicators and iterated through the obtained dataset, using the Nobel Prize dataset to add to each row the number of Nobel Prizes won by the country in the selected year.
We had to merge some rows for the data of Russia, as the country is either named "Russia" or "Russian Federation"depending on the database.
The resulting dataset contained 1,178 samples for 21 columns, including the newly added dependent variable.

We then used these indicators to generate the features we were interested in.
We chose to use absolute values rather than percentages as it is often the case in the indicators.
We also took into account the time between the implementation of a policy and its impact on the Nobel Prize (more details in the section *delayed effects of policies* in the appendix).

The last data cleaning process was to balance the dataset.
Indeed, the dataset included all 19 countries\' data for 62 years, whether they had a Nobel Prize winner that year or not.
With only 195 samples with a win and 983 without, the first models we tried had a tendency to always predict an absence of Nobel Prize.
Thus, we selected all the rows containing a win and added to them an equivalent quantity of randomly sampled losses.
Thus, our final dataset contained around 400 samples.

## Modeling

### Filling Null Value

Although in the previous section, we already filtered out the features with more than 50% of null values, some of the features still have 30%-40% of null values.
Hence, we developed two ways to deal with them.

For the first method, we transformed entire data sets into a binary matrix, where 1 indicates that data point is above the average of the belonging column, and 0 means below.
Since many missing data are from the developing countries, we assume they are all below averages and fill them with 0.
For the other one, we first filled all the null values with its columns' average, then we normalized each column.
There are no significant differences in prediction results between two methods, but from the time complexity perspective, if our group has chances to access the entire world bank dataset with 10,000 features, the first method will be a better option with a shorter time consumption and decent results.

### Bootstrapping

Furthermore, since our group wanted to focus on how research and education expenditure would affect in predicting the Nobel Prize, we selected data after 1996 which was the year that World Bank Data started tracking countries\' expenditure in research and researchers.
However, this led to another big cut in our dataset, and we only had 183 data points after this cut.
Thus, we generated a bootstrapping method on training data for making up this deficiency when we ran the models.

After splitting the dataset into tested and trained dataset, we constructed 5000 new testeded data sets.

Then, we trained the models on each dataset.
We counted all 5000 results, and chose the top N (N = number of winners in the tested set) winners to be our final prediction.
By doing this bootstrapping method, it stabilizes our accuracies in a range between 45.45% and 60% and TPR in a range between 50% and 63.33%, depending on which model we used.

### Adding Features: Existence of Null Value

Based on a suggestion of the professor, we added another feature of whether this data point has null values or not.
This intended to decrease biases of the estimations.
However, since we had two ways for data cleaning, we customized this feature for each one.

For the first method, since it is a binary matrix after data cleaning, we created this feature into a list of 0 and 1, where 1 means this row of data contains null value before data cleaning.
For the other one, this new feature column contains how many null values that each row contains.
In both scenarios, it is hard to conclude the effect of adding this feature in improving the predictions since it created a larger range (41.8%-73%) for accuracy and TPR.

### Modeling with Logistic Regression, LDA and CART

Below is one of the results of the three models with different approaches of data cleaning.
However, this result is not consistent.
Depending on the train-test split and bootstrapping method, each of the models has similar chances to outperform others.
Overall, none of the models has significant statistical power to predict consistently.
Hence, our group came up with some potential adjustments we could make in the future.


|                     | Type I ACC | Type II ACC | Type I TPR | Type II TPR | Type I FPR | Type II FPR |
|---------------------|------------|-------------|------------|-------------|------------|-------------|
| LDA Model           | 0.5636     | 0.4909      | 0.6        | 0.5333      | 0.52       | 0.44        |
| CART Model          | 0.4909     | 0.4909      | 0.5333     | 0.5333      | 0.44       | 0.44        |
| Logistic Regression | 0.5636     | 0.5272      | 0.6        | 0.5666      | 0.52       | 0.48        |

### Potential Improvements
The first one is dealing with the null values.
Instead of filling with the average of each column, we can find the closest datapoint for that country from that year, and then fill the null with that.
Also, when we created the new feature for existence of null value, instead of creating one new feature overall, maybe we can create one new feature for each existing feature.
Then, we should have 2 times features than what we have now, because for each feature, we will create a new one to identify whether it is an actual data or its a fake data by our normalization.

Second, there may exist a better way to aggregate all the results we came up with in the bootstrapping process and perform a final prediction based on that.
The progress we used now may be biased since in the bootstrapping part, it is possible that some of the data points can be randomly selected much more than others.
Thus, there is a larger chance that it will be in the top N of our predictions.
On the other hand, we may also do a bootstrapping on the trained data part to get 5000 different models, and use them all to predict the test set.
Then, we aggregate all 5000 results into a final prediction.

The last one is the need of using a balanced dataset or not.
By the way we aggregated our final model after bootstrapping, it seemed like it is unnecessary to use a balanced dataset.
Since the algorithm will always return the top N (the number of positive in test set) choices in the dataset, there is no worry that the model will classify all data points into negative.

## Conclusion

**Winner\'s Institution Countries VS Original Countries**\
65.22% of Winners did not win the prize from their own country institutions.
23 Original Countries were the same as the winners' institution countries;.
83 Countries in terms of the winner's originality and institution are different (See Appendix (3.1) & (3.2) for visualization)

**Government Spending**\
Within the winning countries, 4.74% of the total Government spending was on Education and 2.32% on Research.

## Appendix

### Nobel Prize attribution to countries

We used the country of death for the four laureates which did not have an organization country and the country of birth for the five which did not have an organization country or a country of death.
A difficulty encountered when dealing with this country data is the evolution of nations over the years.
Several countries that won a Nobel Prize few centuries ago do not exist anymore, for example Prussia, the Austrian Empire or Persia.
Some territories also currently belongs to a different countries than at the time of the winning.
For example Ferdinand Braun won the Physics Nobel Prize in 1909 while working in the University of Strasbourg, while the currently french city of Strasbourg was german.
It was thus Germany which was credited as the Nobel Prize laureate.
Based on these criterias we obtained a list of nineteen countries having won a Nobel Prize (counting USSR and Russia as the same country).

### Selection of relevant indicators:

The program first identify all indicators whose title contained one of the keywords we selected.
Then, it filter the indicators that where specific to one country, one gender or which used quintiles.

![](img/image2.png)

The preselected indicators are then imported for all years and for the nineteen countries having won a Nobel Prize (identified before).
As the WorldBank accept country data only as ISO alpha 2 codes(3), we used another dataset to convert the names to their code.
For the nations that evolved along the years, we assumed that the data of their natural predecessor was stored under the same code (for example that the code RU represents both USSR before 1993 and Russia after 1993).

This operation of importing all pre-selected indicators was very long, as several million of values must be fetched on a distant server.
The output is a list of only 533 preselected indicators, 533 not being importable from the server and 2519 not containing enough data.

![](img/laureates_per_country.jpg)
Number of Nobel Prize won by countries (using the country of the organization in which worked the Nobel Prize laureate)

### delayed effects of policies

For example, if increasing education spendings can increase the chances for a country to train a future Nobel Prize laureate, it will usually take several decades for the children who received this advantage to finish their education and to get the level of expertise required to make a discovery worthy of the prestigious trophy.
Using the STEM Nobel Prize data, we calculated that the laureates have a median of 60 years old with a standard deviation of 12 years.
Thus it makes more sense for a given year to use as education spendings value the education spendings of fifty years ago.

![](img/age_distribution.jpg)

Depending on the indicator, we used the values of the indicator during the years of research of the median Nobel Prize laureate, its years of primary or tertiary school, around his birth or during his whole life (before his win).

This process also provides the advantage to eliminate many null values, as the features use the average of the non-null values over several years.

![](img/country_data.jpg)
![](img/country_data_rolled.jpg)

### Past data analysis

Analyzing past women\'s winners show that they in the United States wins Nobel Prizes regardless of age or year of the award.
Other countries, on the other hand, are more likely to win the older they get.
I\'ve also noticed that their chances of winning an award have increased recently.

#### (1-1) The US for women

![](img/image7.png)

#### (1-2) Other counties for women

![](img/image8.png)

Analysis of past men\'s winners indicates that US winners\' chances of winning declined as they got older.
Other countries, on the other hand, are more likely to win the older they get.
Also, collaborating with other researchers increases their chances of winning an award.

#### (2-1) The US for men

![](img/image9.png)

#### (2-2) Other counties for men

![](img/image10.png)


#### (3-1) Institution countries that were the same as the winners' original countries

![](img/image11.png) 
![](img/image12.png)


23 Original Countries were the same as the winners' institution countries, with USA happening the most times.

#### (3-2) Institution countries that were not the same as the winners' original countries

![](img/image13.png)

![](img/image14.png)

83 Countries in terms of the winner's originality and institution are different, with United Kingdom happening the most.

## References

1. *Nobel prize - laureates* (2017) *opendatasoft*.
Available at: https://public.opendatasoft.com/explore/dataset/nobel-prize-laureates
(Accessed: December 13, 2022).

2. *Indicators* (no date) *The World Bank*.
Available at: https://data.worldbank.org/indicator
(Accessed: December 13, 2022).

3. *ISO 3166-1 alpha-2* (2022) *Wikipedia*. Wikimedia Foundation.
Available at: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
(Accessed: December 13, 2022).
